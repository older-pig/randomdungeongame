# Random Dungeon Game

Random Dungeon Game is a luck-based strategy game. It is unfair currently so don't be afraid to recreate levels on a whim (you won't lose progress).

To start, chmod +x both shell files, first compile it with compile.sh (no special dependencies necessary), then run game.sh any other time.

Currently its Linux only.
