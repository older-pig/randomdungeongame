#include <iostream>
#include <cmath>
#include <random>
#include "const.h"

using namespace std;

class player {
	public: int genesis();
	public: void moveDown(int arr[row][col], int i, int j);
	public: void moveUp(int arr[row][col], int i, int j);
	public: void moveLeft(int arr[row][col], int i, int j);
	public: void moveRight(int arr[row][col], int i, int j);
};
