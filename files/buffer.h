#include <iostream>
#include <termios.h>

class BufferToggle {
	private: struct termios t;
	public: void off(void);
	public: void on(void);
};
