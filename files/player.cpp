#include "player.h"
#include <iostream>
#include <random>
#include <cmath>

int player::genesis() {
	return rand() % (row-2) + 1;
}

void player::moveDown(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i+1][j];
	arr[i+1][j] = pom;
} 

void player::moveUp(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i-1][j];
	arr[i-1][j] = pom;
}

void player::moveLeft(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i][j+1];
	arr[i][j+1] = pom;
}

void player::moveRight(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i][j-1];
	arr[i][j-1] = pom;
}
