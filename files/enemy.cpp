#include <cmath>
#include <random>
#include "enemy.h"

int enemy::distanc(int x,int y,int a,int b) {
	return pow((x-a),2) + pow((y-b),2);
}

int enemy::monsterMovement(int x, int y, int m[row][col], int x2, int y2) {
	int i,d[4],r,max;
	d[0] = distanc(x2+1,y2,x,y);
	d[1] = distanc(x2-1,y2,x,y);
	d[2] = distanc(x2,y2+1,x,y);
	d[3] = distanc(x2,y2-1,x,y);
	max = d[0];
	r = 0;
	for (i = 0; i < 4; i++) {
		if (d[i] < max) {
			max = d[i];
			r = i;
		}
	}
	return r;
}

bool enemy::monsterSpawn(int a[row][col], int i, int j) {
	if (a[i][j] != 1 && a[i][j] != 9) {
		return true;
	} else {
		return false;
	}
}

void enemy::genesis(int g, int h, int arr[row][col]) {
	g = rand() % 13 + 1;
	h = rand() % 13 + 1;
	arr[g][h] = 3;
}

void enemy::moveDown(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i+1][j];
	arr[i+1][j] = pom;
} 

void enemy::moveUp(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i-1][j];
	arr[i-1][j] = pom;
}

void enemy::moveLeft(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i][j+1];
	arr[i][j+1] = pom;
}

void enemy::moveRight(int arr[row][col], int i, int j) {
	int pom;
	pom = arr[i][j];
	arr[i][j] = arr[i][j-1];
	arr[i][j-1] = pom;
}
