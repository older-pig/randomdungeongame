#include "world.h"
#include "enemy.h"
#include "player.h"
#include "buffer.h"
#include <iostream> 
#include <random>
#include <thread>
#include <chrono>
#include <algorithm>
#include <cmath>
#include <time.h>
#include <termios.h>
#include <unistd.h>

using namespace std;

void creation(int arr[row][col], int a, int b, world w, player p, enemy e) {
	hh:
	int g, h, l;
	a = p.genesis();
	w.create(a,arr);
	arr[a][0] = 9;
	int z = 0;
	char oi;
	int k1 = a;
	int k2 = 0;
	bb:
	g = rand() % (row-2) + 1;
	h = rand() % (row-2) + 1;
	arr[g][h] = 3;
	if (!e.monsterSpawn(arr,g,h)) {
		goto bb;
	}
	w.output(arr);
	int k3 = g;
	int k4 = h;
	aa:
	cin>>oi;
	if (oi == 's') {
		if (arr[k1+1][k2] == 1) {
			cout<<"Can't move here. Pick again."<<endl;
			goto aa;
		} else if (arr[k1+1][k2] == 3) {
			z = 1;
			goto gameover;
		} else {
			p.moveDown(arr,k1,k2);
			a += 1;
			k1 = a;
		}
	}
	else if (oi == 'w') {
		if (arr[k1-1][k2] == 1) {
			cout<<"Can't move here. Pick again."<<endl;
			goto aa;
		} else if (arr[k1-1][k2] == 3) {
			z = 1;
			goto gameover;
		} else {
			p.moveUp(arr,k1,k2);
			a -= 1;
			k1 = a;
		}
	}
	else if (oi == 'd') {
		if (arr[k1][k2+1] == 1) {
			cout<<"Can't move here. Pick again."<<endl;
			goto aa;
		} else if (arr[k1][k2+1] == 3) {
			z = 1;
			goto gameover;
		} else {
			p.moveLeft(arr,k1,k2);
			k2 += 1;
		}
	}
	else if (oi == 'a') {
		if (arr[k1][k2-1] == 1) {
			cout<<"Can't move here. Pick again."<<endl;
			goto aa;
		} else if (arr[k1][k2-1] == 3) {
			z = 1;
			goto gameover;
		} else {
			p.moveRight(arr,k1,k2);
			k2 -= 1;
		}
	}
	else if (oi == 'r') {
		cout << "\033[2J\033[1;1H";
		goto hh;
	}
	else if (oi == 'q') {
		exit(42);
	}
	else {
		cout<<"Command non existant. Try again."<<endl;
		goto aa;
	}
	l = e.monsterMovement(k1,k2,arr,k3,k4);
	if (l == 0) {
		if (arr[k3+1][k4] != 1 && arr[k3+1][k4] != 9) {
			e.moveDown(arr,k3,k4);
			k3 += 1;
		} else if (arr[k3+1][k4] == 9) {
		       goto gameover;
	      	}	
	} else if (l == 1) {
		if (arr[k3-1][k4] != 1 && arr[k3-1][k4] != 9) {
			e.moveUp(arr,k3,k4);
			k3 -= 1;
		} else if (arr[k3-1][k4] == 9) {
		       goto gameover;
	      	}	
	} else if (l == 2) {
		if (arr[k3][k4+1] != 1 && arr[k3][k4+1] != 9) {
			e.moveLeft(arr,k3,k4);
			k4 += 1;
		} else if (arr[k3][k4+1] == 9) {
		       goto gameover;
	      	}	
	} else if (l == 3) {
		if (arr[k3][k4-1] != 1 && arr[k3][k4-1] != 9) {
			e.moveRight(arr,k3,k4);
			k4 -= 1;
		} else if (arr[k3][k4-1] == 9) {
		       goto gameover;
	      	}	       
	}
	cout << "\033[2J\033[1;1H";
	w.output(arr);
	if (k2 != (row-1)) {
		goto aa;
	}
	if (z == 1) {
		gameover:
		cout << "\033[2J\033[1;1H";
		cout<<"Game over."<<endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(800));
		exit(42);
	}
}

int main() {
	world w;
	player p;
	enemy e;
	BufferToggle bt;
	int arr[row][col];
	int a,b,br;
	string c;
	char t;
	br = 1;
	vv: 
	cout<<"\033[2J\033[1;1H";
	cout<<"==============================================="<<endl;
	cout<<"The Random Dungeon Game. To win, find the exit."<<endl;
	cout<<"==============================================="<<endl;
	cout<<"                                               "<<endl;
	cout<<"             Press Enter to start.             "<<endl;
	cout<<"                                               "<<endl;
	cout<<"==============================================="<<endl;
	cout<<"Move: w,a,s,d     Main Menu: q     Exit: Ctrl+C"<<endl;
	getline(cin, c);
	bt.off();
	srand(time(NULL));
	while (br <= 10) {
		cout << "\033[2J\033[1;1H";
		cout<<"Level "<<br<<" of 10"<<endl<<c<<endl;
		creation(arr,a,b,w,p,e);
		br++;
	}
	bt.on();
	if (br == 11) {
		cout << "\033[2J\033[1;1H";
		cout<<"You win!"<<endl;
		std::this_thread::sleep_for(std::chrono::milliseconds(2000));
		exit(42);
	}	
}
